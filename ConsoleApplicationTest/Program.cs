﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fibonacci;
using DBWorker;
using NorthwindLibrary;

namespace ConsoleApplicationTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($" Processed value: {Fibonacci.Fibonacci.GetCalculatedValue(5)}");
            Console.WriteLine($" Got value from cache : {Fibonacci.Fibonacci.GetCalculatedValue(4)}");

            DBWorker.DBWorker<Region> worker = new DBWorker.DBWorker<Region>();
            //first request from DB (using System.Runtime.Caching)
            var regions = worker.GetEntities();
            //second request from cache (using System.Runtime.Caching)
            var otherRegions = worker.GetEntities();

            Console.ReadKey();
        }
    }
}
