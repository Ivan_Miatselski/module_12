﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Caching;

namespace Fibonacci
{
    public static class Fibonacci
    {
        private static ObjectCache cache;

        static Fibonacci()
        {
            cache = MemoryCache.Default;
        }

        public static int GetCalculatedValue(int n)
        {
            if (cache.Contains(n.ToString()))
            {
                return (int)cache.Get(n.ToString());
            }
            if (n > 1)
            {
                int value = GetCalculatedValue(n - 1) + GetCalculatedValue(n - 2);
                cache.Set(n.ToString(), value, ObjectCache.InfiniteAbsoluteExpiration);
                return value; 
            }
            else
            {
                return n;
            }
        }
    }
}
